/**
 * Bulls and cows
 * Author: Salif Mehmed - bit.ly/salifm
 * License: MIT
 * Repo: https://github.com/salifm/games/bulls-and-cows/
 */
let ynum = [1, 2, 3, 4, 5, 6, 7, 8, 9].sort(function() {
    return Math.random() - 0.5
}).slice(0, 4); // Your number [n, n, n, n]
let ainum = [1, 2, 3, 4, 5, 6, 7, 8, 9].sort(function() {
    return Math.random() - 0.5
}).slice(0, 4); // AI's number [n, n, n, n]
console.log("AI's number is " + ainum.join("")); // write number to console
let tt = "",
    tta = "",
    pp = false,
    ppa = false,
    guny = 0,
    gunai = 0;
window.$ = _ => document.getElementById(_);
// onkeydown="return kk(this);"
function kk(_) {
    if (_.value.length > 3) {
        if (event.keyCode === 13) {
            ch();
            return false;
        }
        // 13 - enter, 8 - backspace
        if (event.keyCode != 8) {
            return false;
        }
    }
}
// check - onclick="ch();"
function ch() {
    guny++;
    let val = $("inp").value;
    let v = val.split("");
    if (vld(v)) {
        tt = `${val}\ninvalid number!\n\n${tt}`;
    } else {
        let cor = co(ainum, v);
        tt = `${val}\n${cor.bulls} bulls and ${cor.cows} cows\n\n${tt}`;
        if (cor.bulls === 4) {
            pp = true;
        }
    }
    $("yout").innerText = tt;
    $("inp").value = "";
    aiplay();
}
// if (number is valid) { return false }
function vld(_) {
    if ((_[0] != _[1]) && (_[0] != _[2]) && (_[0] != _[3]) && (_[1] != _[2]) && (_[1] != _[3]) && (_[2] != _[3]) && (_[0] > 0 && _[0] < 10) && (_[1] > 0 && _[1] < 10) && (_[2] > 0 && _[2] < 10) && (_[3] > 0 && _[3] < 10)) return false;
    else return true;
}
// count bulls and cows and return {bulls:n, cows:n}
function co(num, guess) {
    let count = {
        bulls: 0,
        cows: 0
    };
    let g = guess.join("");
    for (let i = 0; i < num.length; i++) {
        let dp = g.search(num[i]) != -1;
        if (num[i] == guess[i]) count.bulls++;
        else if (dp) count.cows++;
    }
    return count;
}

function aiplay() {
    gunai++;
    let v = AI();
    if (vld(v)) {
        tta = `${v.join("")}\ninvalid number!\n\n${tta}`;
    } else {
        let cor = co(ynum, v);
        tta = `${v.join("")}\n${cor.bulls} bulls and ${cor.cows} cows\n\n${tta}`;
        if (cor.bulls === 4) {
            ppa = true;
        }
    }
    $("aiout").innerText = tta;
    if (pp || ppa) {
        gameover();
    }
}

function gameover(_) {
    let fout = "";
    if (pp && ppa) {
        fout = "Draw!";
    } else if (pp) {
        fout = "You won!";
    } else if (ppa) {
        fout = "You lose!";
    }
    $("vdiv").innerHTML = `
    <h1>${fout}</h1>\n
    <input type="button" value="new game" class="new-game" onclick="ng();" />
    `;
}
// new game
function ng() {
    ynum = [1, 2, 3, 4, 5, 6, 7, 8, 9].sort(function() {
        return Math.random() - 0.5
    }).slice(0, 4);
    ainum = [1, 2, 3, 4, 5, 6, 7, 8, 9].sort(function() {
        return Math.random() - 0.5
    }).slice(0, 4);
    console.log("AI's number is " + ainum.join(""));
    tt = "";
    tta = "";
    pp = false;
    ppa = false;
    $("yout").innerText = "";
    $("aiout").innerText = "";
    $("vdiv").innerHTML = `
    <input type="number" id="inp" onkeydown="return kk(this);" /><input type="button" class="check" value="check" onclick="ch();" />
    <br /><br />
    `;
    $("pdiv").innerHTML = `
    Your number : ${ynum.join("")}
    <br /><br />
    AI&apos;s number : $$$$
    `;
}
window.addEventListener("load", () => {
    $("pdiv").innerHTML = `
    Your number : ${ynum.join("")}
    <br /><br />
    AI&apos;s number : $$$$
    `;
});
// return random number [n, n, n, n]
function AI() {
    return [1, 2, 3, 4, 5, 6, 7, 8, 9].sort(function() {
        return Math.random() - 0.5
    }).slice(0, 4);
}